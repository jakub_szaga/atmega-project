/*
 * main.c
 *  Created on: 4 sty 2017
 *  Author: Jan Swarthoff, Jakub Szaga
 * 	The functions which are supporting external elements are from different websites links below
 *  http://www.leniwiec.org/2014/04/28/enkoder-obrotowy-w-praktyce-prawidlowe-podlaczenie-do-mikrokontrolera-avr/  <- Encoder
 *  https://hackaday.com/2008/12/27/parts-8bit-io-expander-pcf8574/ <-I2C usage theory
 *  http://www.ermicro.com/blog/?p=1239 <-I2C funcions
 *  http://are.net.pl/ <- LCD
 */


#define F_CPU 8000000UL
#include <avr/io.h>
#include <stdio.h>
#include <util/delay.h>
#include <string.h>
#include <compat/twi.h>
#include <avr/interrupt.h>
#define pcf8574_adres 0b01000000
#define UART_BAUD 9600						/* serial transmission speed */
#define UART_CONST F_CPU/16/UART_BAUD-1

int BREAKDOWN=0;

void delay_ms(int ms) //Delay in miliseconds
    {
    volatile long unsigned int i;
    for(i=0;i<ms;i++)
        _delay_ms(1);
    }

void delay_us(int us) //Delay in microseconds
    {
    volatile long unsigned int i;
    for(i=0;i<us;i++)
        _delay_us(1);
    }

void buzzing(int ms)
{
	PORTD |= (1 << PD7); 
	_delay_ms(ms); 	 
	PORTD ^= 0x80; 
}

#define RS 0
#define RW 1
#define E  2

void UART_init(unsigned int ubrr)
{
	UBRRH = (unsigned char)(ubrr>>8);
	UBRRL = (unsigned char)ubrr;

	/* Enable receiver and transmitter */
	UCSRB = (1<<RXEN)|(1<<TXEN)|(1<<RXCIE);
	/* Set frame format: 8data, 2stop bit */
	UCSRC = (1<<URSEL)|(1<<USBS)|(3<<UCSZ0);
} // end of UART_init()

void i2c_init(void)
{
	TWSR = 0;    
	TWBR = 0x10;   
}

unsigned char i2c_start(unsigned char adres)
{
	uint8_t twst;
	// send START condition
	TWCR = (1<<TWINT) | (1<<TWSTA) | (1<<TWEN);
	// wait until transmission completed
	while(!(TWCR & (1<<TWINT)));
	// check value of TWI Status Register. Mask prescaler bits.
	twst = TW_STATUS & 0xF8;
	if ( (twst != TW_START) && (twst != TW_REP_START)) return 1;
	// send device address
	TWDR = adres;
	TWCR = (1<<TWINT) | (1<<TWEN);
	// wail until transmission completed and ACK/NACK has been received
	while(!(TWCR & (1<<TWINT)));
	// check value of TWI Status Register. Mask prescaler bits.
	twst = TW_STATUS & 0xF8;
	if ( (twst != TW_MT_SLA_ACK) && (twst != TW_MR_SLA_ACK) ) return 1;

	return 0;
}

/*** Sendig a bit yo the magistrale I2C ***/
unsigned char i2c_write( unsigned char data )
{
	uint8_t twst;
	// send data to the previously addressed device
	TWDR = data;
	TWCR = (1<<TWINT) | (1<<TWEN);
	// wait until transmission completed
	while(!(TWCR & (1<<TWINT)));
	// check value of TWI Status Register. Mask prescaler bits
	twst = TW_STATUS & 0xF8;
	if( twst != TW_MT_DATA_ACK) return 1;
	return 0;
}

void i2c_stop(void)
{
	/* send stop condition */
	TWCR = (1<<TWINT) | (1<<TWEN) | (1<<TWSTO);
	// wait until stop condition is executed and bus released
	while(TWCR & (1<<TWSTO));
}

unsigned char i2c_readAck(void)
{
	TWCR = (1<<TWINT) | (1<<TWEN) | (1<<TWEA);
	while(!(TWCR & (1<<TWINT)));
	return TWDR;
}

unsigned char i2c_readNak(void)
{
	TWCR = (1<<TWINT) | (1<<TWEN);
	while(!(TWCR & (1<<TWINT)));
	return TWDR;
}

uint8_t read_gray(void ) // Reading data from the encoder
{
 uint8_t val=0;
 if(!bit_is_clear(PIND, PD2))
	val |= (1<<1);

 if(!bit_is_clear(PIND, PD3))
	val |= (1<<0);

  return val;
}

void LCD2x16_init(void)//initiation of the LCD
{
	PORTB &= ~(1<<RS);
	PORTB &= ~(1<<RW);

	PORTB |= (1<<E);
	PORTA = 0x38;   
	PORTB &=~(1<<E);
	_delay_us(120);

	PORTB |= (1<<E);
	PORTA = 0x0e;   
	PORTB &=~(1<<E);
	_delay_us(120);

	PORTB |= (1<<E);
	PORTA = 0x06;
	PORTB &=~(1<<E);
	_delay_us(120);
}

void LCD2x16_clear(void)//Screen clearing
{
	PORTB &= ~(1<<RS);
	PORTB &= ~(1<<RW);

	PORTB |= (1<<E);
	PORTA = 0x01;
	PORTB &=~(1<<E);
	delay_ms(120);
}

void LCD2x16_putchar(int data)//Sending data to display
{
	PORTB |= (1<<RS);
	PORTB &= ~(1<<RW);

	PORTB |= (1<<E);
	PORTA = data;
	PORTB &=~(1<<E);
	_delay_us(120);
}

void LCD2x16_pos(int wiersz, int kolumna)//Choosing the line to display
{
	PORTB &= ~(1<<RS);
	PORTB &= ~(1<<RW);

	PORTB |= (1<<E);
	delay_ms(1);
	PORTA = 0x80+(wiersz-1)*0x40+(kolumna-1);
	delay_ms(1);
	PORTB &=~(1<<E);
	_delay_us(120);
}

int Keyboard(void)
{ 

	int pressed=13;
	uint8_t pro =0b11101111;
	unsigned char data;
	i2c_start(pcf8574_adres);
	i2c_write(0x40);
	i2c_write(pro);
	i2c_stop();
	i2c_start(0x41);
	data=i2c_readNak();
	i2c_stop();
	asm volatile("nop");
    if(data==0b11101110)
        pressed=1;
    if(data==0b11101101)
        pressed=4;
    if(data==0b11101011)
        pressed=7;
	if(data==0b11100111)
	{
		pressed=10;
		buzzing(25);
	}
	pro =0b11011111;
	i2c_start(pcf8574_adres);
	i2c_write(0x40);
	i2c_write(pro);
	i2c_stop();
	i2c_start(0x41);
	data=i2c_readNak();
	i2c_stop();
	asm volatile("nop");
    if(data==0b11011110)
        pressed=2;
    if(data==0b11011101)
        pressed=5;
    if(data==0b11011011)
        pressed=8;
	if(data==0b11010111)
		pressed=0;
	pro =0b10111111;
	i2c_start(pcf8574_adres);
	i2c_write(0x40);
	i2c_write(pro);
	i2c_stop();
	i2c_start(0x41);
	data=i2c_readNak();
	i2c_stop();
	asm volatile("nop");
    if(data==0b10111110)
        pressed=3;
    if(data==0b10111101)
        pressed=6;
    if(data==0b10111011)
        pressed=9;
	if(data==0b10110111)
	{
		pressed=11;
		buzzing(25);
	}

	return pressed;

}

int Keyboard_input(int arg)
{
	long int temporary=0;
	long int value=0;
	int help = 13;
	int input=0;
	int numbers[6] = {0,0,0,0,0,0};
	int j =0;
	char tmp[16];
	int i=0;
	long int MAX;
	long int przedp=0;
	int temporary1=0;
	int temporary2=0;

	if(arg==1)
		MAX=99999;
	else if(arg==2)
		MAX=9999;

	LCD2x16_clear();
	while(1)
    {
		sprintf(tmp,"Zatwierdz ziel! ");
		for(i=0;i < 16;i++)
			LCD2x16_putchar(tmp[i]);
		LCD2x16_pos(2,1);
		if(arg==1)
		sprintf(tmp,"price:%li.%i%iPLN", przedp,temporary1,temporary2);
		else if(arg==2)
		sprintf(tmp,"amount:%li.%i%iL ", przedp,temporary1,temporary2);
		for(i=0;i < 16;i++)
			LCD2x16_putchar(tmp[i]);

		help = 13;

		while(help==13)
			help=Keyboard();

		input=help; //nacisniecie Keyboardu

		if(input!=10 && input !=11)
		{
			if(value <= MAX)
			{
				if(j==0 && input == 0)
				{
				}
				else{
				value = value*10+input;
				numbers[j]=input;
				j++;
				}
			}
		}
		else if(input == 11)
		{
			if(j==0)
			{
			return value;
			}
			if(j>0)
			{
			j--;
			value = (value-numbers[j])/10;
			}
		}
		else if(input == 10)
		{
			LCD2x16_clear();
			sprintf(tmp,"Nalewam fueliwo!");
			for(i=0;i < 16;i++)
				LCD2x16_putchar(tmp[i]);
			LCD2x16_pos(2,1);
			if(arg==1)
				sprintf(tmp,"price:%li.%i%iPLN ",przedp,temporary1,temporary2);
			else if(arg==2)
				sprintf(tmp,"amount:%li.%i%iL  ",przedp,temporary1,temporary2);
			for(i=0;i < 16;i++)
				LCD2x16_putchar(tmp[i]);
			delay_ms(100);
			return value;
		}
		delay_ms(30);

		if(j>2)
		{
			temporary = 10*numbers[j-2]+numbers[j-1];
			temporary1=numbers[j-2];
			temporary2=numbers[j-1];
			przedp = value - temporary;
			przedp = przedp/100;
		}
		else if(j==2)
		{
			temporary =10*numbers[j-2]+numbers[j-1];
			temporary1=numbers[j-2];
			temporary2=numbers[j-1];
			przedp = 0;
		}
		else if(j==1)
		{
			temporary =numbers[j-1];
			temporary1=0;
			temporary2=temporary;
			przedp=0;
		}
		else if(j==0)
		{
			temporary = 0;
			przedp = 0;
			temporary1=0;
			temporary2=0;
		}
	}
}

int pouring(long int value)
{
	uint8_t val=0, val_tmp =0;
	int help;
	float poured=0;
	while(value>=poured) 
   {
		help=Keyboard();
	if(bit_is_set(PINB,3) && bit_is_clear(PIND,6) && help!=12)
		{
		val_tmp = read_gray(); 
		if(val != val_tmp)
		{
			if((val==3 && val_tmp==1)||(val==0 && val_tmp==2))
				poured++;
			val = val_tmp;
		}               
		_delay_ms(1);
		}
	else
	{
		if(help==11)
		{
			BREAKDOWN=1;
			break;
		}
		else
			break;
	}
   }
return poured;
}

int Pouring_Process(long int value)
{
	char tmp[16];
	float poured = 0;
	float sum=0;
	int i;
	long int reply=value;
	while(bit_is_clear(PIND,6))
	{
		if(bit_is_clear(PINB,3))//waz odlaczono
		{
			LCD2x16_clear();
			sprintf(tmp,"NIE SKONCZYLEM! %i", poured);
			for(i=0;i < 16;i++)
				LCD2x16_putchar(tmp[i]);
			LCD2x16_pos(2,1);
			sprintf(tmp,"PODLACZ WAZ!");
			for(i=0;i < 16;i++)
				LCD2x16_putchar(tmp[i]);
			delay_ms(100);
		}
		if(BREAKDOWN==1)// czerwony guzik
		{
			LCD2x16_clear();
			sprintf(tmp,"Wstrzymano ...");
			for(i=0;i < 16;i++)
				LCD2x16_putchar(tmp[i]);
			LCD2x16_pos(2,1);
			sprintf(tmp,"pouring fueliwa");
			for(i=0;i < 16;i++)
				LCD2x16_putchar(tmp[i]);
			delay_ms(100);
			break;
		}
		else
		{
			PORTD ^= (1 << PD5); 
			poured = pouring(reply);
			PORTD ^=0x20;
			if(reply<=0)
			{
				break;
			}
			else
			{
				reply=reply-poured;
				sum=sum+poured;
			}
		}
	}
	return sum;
}

int main(void){
	//variable section
	char www[16] = "Witaj na stacji!";
	char email[16] = "Umiesc waz w bak";
	char tmp[16];
	char blad[16] = "Nie ma opcji!";
	float help=0;
	float reply=0;
	float price;
	float amount;
	float price_fuel=4.5;
	float poured=0;
	int i;
	// Port section
	DDRA  =0xff; 
	PORTA =0xff;
	DDRB  =0xff;
	PORTB =0x00;
	UART_init(UART_CONST);
	DDRD &=~ (1 << PD2);	
	DDRD &=~ (1 << PD3); 
	DDRD =0xf3;
	PORTD |= (1 << PD3)|(1 << PD2); 
	PORTD ^=0x20;
	DDRC =0xff;
	PORTC=0x01;
	_delay_ms(50);
	i2c_init();
	LCD2x16_init();
	jump_place:
	LCD2x16_clear();

	while(1)
	{
	BREAKDOWN=0;
	reply=0;
	sprintf(www, "Witaj na stacji! ");
	sprintf(email,"Umiesc waz w bak ");
	for(i=0;i < 16;i++)
        LCD2x16_putchar(www[i]);
    LCD2x16_pos(2,1);
    for(i=0;i < 16;i++)
        LCD2x16_putchar(email[i]);

	delay_ms(100);

	if(bit_is_set(PINB,3))
	{
		LCD2x16_clear();
		sprintf(www, "Waz podlaczony! ");
		sprintf(email,"Zatwierdz ziel. ");
		for(i=0;i < 16;i++)
			LCD2x16_putchar(www[i]);
		LCD2x16_pos(2,1);
		for(i=0;i < 16;i++)
			LCD2x16_putchar(email[i]);
		delay_ms(10);
		while(help != 10)
		{
			if(bit_is_clear(PINB,3))
			{
				goto jump_place;
			}
			help=Keyboard();
		}
		if(help==10)
		{
			while(reply==0)
			{
				help = 13;
				LCD2x16_clear();
				sprintf(tmp,"Metoda: 1.price");
				for(i=0;i < 16;i++)
					LCD2x16_putchar(tmp[i]);
				LCD2x16_pos(2,1);
				sprintf(tmp,"2.amount 3.FULL");
				for(i=0;i < 16;i++)
					LCD2x16_putchar(tmp[i]);
				delay_ms(100);
				while(help==13)
				{
					help=Keyboard();
					if(bit_is_clear(PINB,3)/*waz_podlaczony==1*/)
					{
						goto jump_place;
					}
				}
				switch(help)
				{
					case 1:
					LCD2x16_clear();
					reply=Keyboard_input(1);
					price = reply;
					amount = price/price_fuel;
					break;

					case 2:
					LCD2x16_clear();
					reply=Keyboard_input(2);
					amount = reply;
					price = amount*price_fuel;
					break;

					case 3:
					LCD2x16_clear();
					reply=999999;
					price=999999;
					amount=price/price_fuel;
					break;

					default:
					LCD2x16_pos(2,1);
					for(i=0;i < 16;i++)
						LCD2x16_putchar(blad[i]);
					delay_ms(100);
					break;
				}
			}

			float przedp=0;
			float temporary1=0;
			float temporary2=0;

			if(price<10)
			{
				temporary2=price;
			}

			if(price<100 && price>9)
			{
				temporary2=price%10;
				temporary1=(price-temporary2)/10;
			}

			if(price>99)
			{
				temporary2=price%10;
				temporary1=((price%100)-temporary2)/10;
				przedp=(price-(price%100))/100;
			}

			if(!bit_is_set(PINB,3))
			{
				goto jump_place;
			}

			if(price==999999)
			{
				LCD2x16_clear();
				sprintf(tmp,"Nalewam fueliwo! ");
				for(i=0;i < 16;i++)
					LCD2x16_putchar(tmp[i]);
				LCD2x16_pos(2,1);

				sprintf(tmp,"Nalewam do pelna");
				for(i=0;i < 16;i++)
					LCD2x16_putchar(tmp[i]);
				delay_ms(100);
			}
			else
			{
				LCD2x16_clear();
				sprintf(tmp,"Nalewam fueliwo! ");
				for(i=0;i < 16;i++)
					LCD2x16_putchar(tmp[i]);
				LCD2x16_pos(2,1);

				sprintf(tmp,"P.price:%li.%i%i",przedp,temporary1,temporary2);
				for(i=0;i < 16;i++)
					LCD2x16_putchar(tmp[i]);
				delay_ms(100);
			}

			poured=Pouring_Process(amount);

			price = poured*price_fuel;

			if(price<10)
			{
				temporary1=0;
				przedp=0;
				temporary2=price;
			}
	
			if(price<100 && price>9)
			{
				przedp=0;
				temporary2=price%10;
				temporary1=(price-temporary2)/10;
			}
			if(price>99)
			{
				temporary2=price%10;
				temporary1=((price%100)-temporary2)/10;
				przedp=(price-(price%100))/100;
			}
	
			LCD2x16_clear();
	
			while(bit_is_set(PINB,3))
			{
				if(bit_is_set(PIND,6))//warunek pelnego baku
				{
					sprintf(tmp,"poured do pelna!");
				}
				else
				{
				sprintf(tmp,"poured pod.amount");
				}
				for(i=0;i < 16;i++)
					LCD2x16_putchar(tmp[i]);
				LCD2x16_pos(2,1);
				sprintf(tmp,"Odlacz waz z bak");
				for(i=0;i < 16;i++)
					LCD2x16_putchar(tmp[i]);
				delay_ms(100);
			}
			
			LCD2x16_clear();
			sprintf(tmp," Do zaplaty: ");
			for(i=0;i < 16;i++)
				LCD2x16_putchar(tmp[i]);
			LCD2x16_pos(2,1);

			sprintf(tmp,"%li.%i%i",przedp,temporary1,temporary2);
			for(i=0;i < 16;i++)
				LCD2x16_putchar(tmp[i]);

			delay_ms(100);

			LCD2x16_clear();
			sprintf(tmp," Zaplac w kasie ");
			for(i=0;i < 16;i++)
				LCD2x16_putchar(tmp[i]);
			LCD2x16_pos(2,1);

			sprintf(tmp,"lub tutaj karta.");
			for(i=0;i < 16;i++)
				LCD2x16_putchar(tmp[i]);

			delay_ms(100);

			LCD2x16_clear();
			sprintf(tmp,"Dzieki za wybor ");
			for(i=0;i < 16;i++)
				LCD2x16_putchar(tmp[i]);
			LCD2x16_pos(2,1);

			sprintf(tmp," naszej stacji! ");
			for(i=0;i < 16;i++)
				LCD2x16_putchar(tmp[i]);

			delay_ms(100);
		}
	}
}
return 0;

}
